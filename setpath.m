function setpath

p_ = pwd;

% libraries:
addpath([p_ '/lib/MirandaFackler']);
addpath([p_ '/lib/Reiter/appraggr']);
addpath([p_ '/lib/Reiter/appraggr/libaa']);
addpath([p_ '/lib/Reiter/appraggr/libm']);
addpath([p_ '/lib/Lengwiler']);
addpath([p_ '/lib/dynare']);
addpath([p_ '/model']);

end