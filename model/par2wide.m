% File: par2wide.m
% Author: Alisdair McKay
% July 19, 2014
%
% Description: converts policy rule parameter vector to matrix
%
% Feel free to use, copy or modify this program in any way.

function par = par2wide(par)



global Params;

nn = Params.nn;
nc = Params.nc;
npp = Params.npp;


[m, n] = size(par);


if m == nn+nc && n == npp
    return
end


if (m == nn*npp + nc*npp && n ==1)
    par = [reshape(par(1:nn*npp), nn, npp);
        reshape(par(npp*nn+1:end),nc,npp)];
else
    error(['format of par supplied to par2wide is not recognized' num2str(m) ' ' num2str(n)])
end

