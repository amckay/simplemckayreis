% compute transition matrix
% Pi(i,j) is probability to go from state j to state i!
% modified 9/24/12 by AGM
function Pi = forwardmat(iFullMat,par)
global Params;

nd = Params.ndstst;
IR = []; IC = []; VV = [];


for ip=1:Params.npp  % this period's state
    
    
    S = savingspline(par(Params.par_sind,ip));
    Kend = interp_savspline(S,Params.knotDistrK);
    
    
    
    %--
    
    Ti = lineartrans(Params.knotDistrK,Kend);
    ir{ip} = Ti.iTo;  % row indicates to which position we go to
    ic{ip} = Ti.iFr;  % column indicates which position we come from
    vv{ip} = Ti.Val;
    
    
    if(iFullMat)
        offsi = (ip-1)*nd;
        for jp=1:Params.npp  % next period's state
            
            pp = transProb(ip,jp);

            
            if(any(pp>0))
                offsj = (jp-1)*nd;
                IR = [IR;offsj+ir{ip}];  % where to go! take offsi
                IC = [IC;offsi+ic{ip}];  % where come from! take offsi
                VV = [VV;pp.*vv{ip}];
            end
        end
    else
        Pij{ip} = sparse(ir{ip},ic{ip},vv{ip},nd,nd);
    end
end  %end loop over this period state

if(iFullMat)
    nn = nd*Params.npp;
    Pi = sparse(IR,IC,VV,nn,nn);
else
    transMat = Params.transMat_ss + Params.transMat_z * labmktstat;
    Pi = op_concat(op_kron(newTransProb',speye(nd)),blockdiag(Pij));  %the ordering here matters for sequence of taking saving decision then learning new skill/employment status rather then vice versa as in Reiter's model.
end




