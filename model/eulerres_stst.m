function z = eulerres_stst(par,R,wage,tau)

  logaggz = 0;
  ppi = 1;
  taxshock = 0;

  z = eulerres(par,par,R,R,wage,wage,ppi,tau,tau);
  