% File: par2long.m
% Author: Alisdair McKay
% July 19, 2014
% 
% Description: reformats the parameter vector from wide to long (i.e. from
% a matrix to a vector)
%
% Inputs: parameter vector in wide or long format
%
% Ouputs: parameter vectonr in long format
%
%
% Feel free to use, copy or modify this program in any way.

function par = par2long(par)



global Params;

nn = Params.nn;
nc = Params.nc;
npp = Params.npp;


[m, n] = size(par);


    if (m == nn*npp + nc * npp && n == 1)
        return
    end

    assert(m == nn+nc && n == npp);

    par = [ reshape(par(1:nn,:),npp*nn,1);
        reshape(par(nn+1:end,:),npp*nc,1)];



