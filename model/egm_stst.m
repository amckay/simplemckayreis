function par = egm_stst(par,R,wage,tau,T)
  %compute steady state savings and labor supply using endog grid method
  
  
  

  
parPath = solveback(par2long(par),ones(T,1),repmat(wage,T,1),repmat(R,T,1),repmat(tau,T,1));
par = par2wide(parPath(:,1));
    
    
end

