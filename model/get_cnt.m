% File: get_cnt.m
% Author: Alisdair McKay
% July 19, 2014
% 
% Description: gets consumption and labor supply from household budget
% constraint, savings amount and labor supply policy/level.
%
% Inputs: 
% b assets (vector)
% savings (vector)
% N  labor spline or vector of labor supplies
% R, wage prices
%
% Ouputs: consumption and labor levels
%
%
% Feel free to use, copy or modify this program in any way.

function [c, n] = get_cnt(b,savings,N,R,wage,tau,incomeIndex)


global Params;

effectiveWage = (1-tau)*Params.skill(incomeIndex)*wage;

if isstruct(N)
    n = interp_nspline(N,b,true); %true -> take max of zero
else
    n = max(N,0);
end

c = R*b + effectiveWage*n  - savings;
