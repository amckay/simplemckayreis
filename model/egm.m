% File: egm.m
% Author: Alisdair McKay
% July 19, 2014
%
% Description: One iteration of the endogenous grid point method.
%
% Inputs:parameters next period, prices, and
%
% Ouputs:
%
%
% Feel free to use, copy or modify this program in any way.

function parNew = egm(par,R,Rnext,wage,wagenext,ppinext,ppinextnext,tau,taunext)
%par = egm_c(par,R,Rnext,wage,wagenext,ppinext,labormarketstatus)
%iterate once on consumption and labor supply using the endog grid method

global Params;


npp = Params.npp;


% Section 1: solve for savings rule using EGM

% asset grid
bthis = [0; Params.knotXi];


nassets = length(bthis);
nc = Params.nc;
assert(nc == nassets);

%compute consumption and marg util for each asset level and income level
MU = NaN(nassets,npp);
nthis = NaN(nassets,npp);

for jp=1:npp
    
    
    S = savingspline(par(Params.par_sind,jp));
    N = nspline(par(Params.par_nind,jp));
    
    sthis = interp_savspline(S,bthis);
    
    %we have end of period assets in last period and this period,
    %and we have to figure out this period's consumption:
    [cthis, nthis(:,jp)] = get_cnt(bthis,sthis*ppinextnext,N,Rnext,wagenext,taunext,jp);
    
    
    if ~ all(cthis>0)
        disp('negative c')
        jp
        I = cthis <=0;
        find(I)
        cthis(I)
        nthis(I,jp)
        bthis(I)
        sthis(I)
    end
    assert(all(cthis>0));
    
    MU(:,jp) = margutilC(cthis);
end


%compute expected marg util
MUexp = zeros(nassets,npp);
for ip=1:npp  %loop over previous income states
    ppsum = 0;
    for jp = 1:npp %loop over this period income state
        pp = transProb(ip,jp);
        if(pp>0)
            MUexp(:,ip) = MUexp(:,ip) + pp .* MU(:,jp) .* Rnext/ppinext;
            ppsum = ppsum  + pp;
        end
    end
    MUexp(:,ip) = MUexp(:,ip)/ppsum;
    assert(abs(ppsum-1) < 1e-9)
end


clear MU;

Cprev = invmargutilC(Params.betah*MUexp);
assert(all(Cprev(:) > 0))


%at this stage we know the previous c and n on the b' grid, but we
%don't know n or b
assert(all(size(Cprev) == [Params.nc, Params.npp]))
bprev = egm_bn(repmat(bthis,1,Params.npp),nthis,Cprev,repmat(bthis,1,Params.npp),R-1,ppinext,wage,tau);



%pack results back into par
parNew =  NaN(size(par));

parNew(Params.par_sind(1),:) = bprev(1,:);
for ip = 1:npp
    
    XX = [bprev(:,ip); 1e8];
    tmp = (bthis(end)-bthis(end-1))/(bprev(end,ip)-bprev(end-1,ip))*(1e8-bprev(end,ip)) + bthis(end);
    YY = [bthis; tmp];
    
    parNew(Params.par_sind(2:end),ip) = interp1(XX,YY,bprev(1,ip) + Params.knotXi,'linear');
    
    %if ip == 3
    %    [bprev(end-8:end,ip)'; xthis(end-8:end)';bprev(1,ip) + Params.knotXi(end-8:end)'; parNew(Params.par_sind(end-8:end),ip)']
    %end
end


%% Section 2 --  Solve for n rule using static first order condition
% we have already done this before, but we may not have solved for the
% decision rule in all parts of the state space

bthis = Params.ngrid;
savings = zeros(Params.nn,npp);

nnew = zeros(Params.nn,npp);
for ip=1:npp
    
    S = savingspline(parNew(Params.par_sind,ip));
    savings(:,ip) = interp_savspline(S,bthis);
    
    N = nspline(par(Params.par_nind,ip));  %for an initial guess
    nnew(:,ip) = interp_nspline(N,bthis,true);
    
end


nnew = solveN(nnew, repmat(bthis,1,npp),savings*ppinext,R-1,wage,tau);

parNew(Params.par_nind,:) = nnew;




end


function [b , n  ] = egm_bn(b0,n0,c,bprime,ii,ppi_prime,wage,tau)
% solves non-linear equation for previous assets and labor given previous c


global Params;

na = size(b0,1);

skill = repmat(Params.skill,na,1);


b = b0;
n = n0;


for it = 1:100
    
    
    f1 = (1 + ii) * b + (1-tau)*skill.*wage.*n -c - bprime*ppi_prime;
    
    
    f2 = (1-tau).*wage.*skill.*margutilC(c) - Params.psi1*n.^(Params.psi2);
    
    if all(abs([f1(:);f2(:)]) < 1e-10)
        break
    end
    
    J1b = repmat(1+ii,na,Params.npp);
    J1n = (1-tau).*skill*wage;
    
    J2b = zeros(na,Params.npp);
    J2n =  - Params.psi1*Params.psi2.*n.^(Params.psi2-1);
    
    D = NaN(na,Params.npp,2);
    for ip = 1:Params.npp
        
        for ia = 1:na
            J = [J1b(ia,ip) J1n(ia,ip); J2b(ia,ip) J2n(ia,ip)];
            if cond(J) > 1e10
                J
                n(ia,ip)
                skill(ia,ip)
                pause;
            end
            D(ia,ip,:) = -[J1b(ia,ip) J1n(ia,ip); J2b(ia,ip) J2n(ia,ip)]\[f1(ia,ip);f2(ia,ip)];
        end
        n(:,ip) = n(:,ip) + D(:,ip,2);
        
    end
    
    b = b + D(:,:,1);
    
    
    
end

if ~all(abs([f1(:);f2(:)]) < 1e-10)
    max(abs(f1))
    max(abs(f2))
end
assert(all(abs([f1(:);f2(:)]) < 1e-10))
assert(it < 100)

end

function [n] = solveN(n0, b,savings,ii,wage,tau)
% solves non-linear equation for labor given assets and savings


global Params;

na = size(n0,1);

skill = repmat(Params.skill,na,1);

n = n0;


for it = 1:100
    
    c = (1+ii)*b + (1-tau)*skill.*wage.*n - savings;
    
    
    f2 = (1-tau).*wage.*skill.*margutilC(c) - Params.psi1*n.^(Params.psi2);
    
    if all(abs(f2(:)) < 1e-14)
        break
    end
    
    
    
    J2n = margutilC(c,2) .* (skill.*wage.*(1-tau)).^2 ...
        - Params.psi1*Params.psi2.*n.^(Params.psi2-1);
    
    D = -f2./J2n;
    
    n = n + D;
    
    
    
    
    
end

end

