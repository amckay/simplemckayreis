%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCTION THAT DEFINES EULER AND LABOR SUPPLY RESIDUALS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% inputs:
%   par:           parameter vector for_ current savings function_
%   parnext:       parameter vector for_ next periods' savings function_
%   R,wage: 1+interest,wage paid this period.  i.e interest on assets entering the period
%   Rnext,wagenext: 1+interest,wage paid next period
%   steadyState:   T/F are we computing the steady state?
%   xthis:        grid at which_ to compute Euler residuals; default: knot points of spline
% output:
function res = eulerres(par,parnext,R,Rnext,wage,wagenext,ppinext,tau,taunext,xthis)

global Params;


npp = Params.npp;

par = par2wide(par);
parnext = par2wide(parnext);



%figure out if we are doing a test of residuals off the grid
IsATest = exist('xthis','var');

if ~IsATest
    nc = Params.nc;
    nn = Params.nn;
else
    nc = length(xthis);
    nn = nc;
end


for ip=1:npp
    
    S = savingspline(par(Params.par_sind,ip));
    N = nspline(par(Params.par_nind,ip));
    
    
    
    if IsATest % xthis given
        sthis = interp_savspline(S,xthis);
    else % take x as starting value of assets
        xthis = S.x(1:end-1);  % last point in S is for interpolation
        sthis = S.y(1:end-1);
    end
    
    
    
    %we have end of period assets in last period and this period,
    %and we have to figure out this period's consumption:
    cthis = get_cnt(xthis,sthis,N,R,wage,tau,ip);
    
    
    
    if(any(cthis<0))  %signal inadmissible value to routine 'broydn';
        error('inadmiss')
        %         ip
        %         I = find(cthis<0)
        %         nthis(I)
        %         xthis(I)
        %         sthis(I)
        res = 1e100;
        return;
    end
    
    
    if(ip==1) % initialize z:
        res = initsize(zeros(nc+nn,Params.npp),par,parnext,R,Rnext,wage,wagenext,tau,taunext,ppinext,xthis);
    end
    
    
    assets = sthis;
    
    MUexp = 0;
    
    
    for jp=1:npp
        
        pp = transProb(ip,jp);
        
        
        if(any(pp>0))
            Sn = savingspline(parnext(Params.par_sind,jp));
            Nn = nspline(parnext(Params.par_nind,jp));
            
            
            snext = interp_savspline(Sn,assets);
            
            
            cnext = get_cnt(assets,snext,Nn,Rnext,wagenext,taunext,jp);
            
            
            
            if(any(cnext<1e-8))  %signal inadmissible value to routine 'broydn';
                error('inadmiss')
                res = 1e100;
                return;
            end
            
            MUnext = margutilC(cnext);
            
            
            MUexp = MUexp + pp.*MUnext .* Rnext/ppinext;
            
            
        end
    end
    
    
    % Euler residual:
    % expressed in relative consumption units:
    %notice that R is incorporated into MUexp
    
    res(nn+1:end,ip) = 1 - invmargutilC(Params.betah*MUexp)./cthis;
    
    
    
    %calculations for labor supply residuals
    clear nthis sthis cthis;
    
    if IsATest % xthis given
        nthis1 = interp_nspline(N,xthis,false);
    else % take x as starting value of assets
        xthis = N.x(1:end-1);  % last point in N is for interpolation
        nthis1 = N.y(1:end-1);
    end
    
    sthis = interp_savspline(S,xthis);
   
    
    
    cthis = get_cnt(xthis,sthis,N,R,wage,tau,ip);
    
    
    if(any(cthis<0))  %signal inadmissible value to routine 'broydn';
        %           disp('C');
        res = 1e100;
        return;
    end
    
    
    laborResid =  Params.psi1 .* nthis1.^Params.psi2   ./(cthis.^(-Params.sigma).*Params.skill(ip)*wage.*(1-tau))-1;  %notice that n is allowed to be negative only here
    
    
    %store results
    res(1:nn,ip) = laborResid;
    
    
    clear nthis sthis cthis;
    
    
    
    
end  %close loop over this period's income state


res = [ reshape(res(1:nn,:), nn*Params.npp,1);
    reshape(res(nn+1:nn+nc,:), nc*Params.npp,1)];

