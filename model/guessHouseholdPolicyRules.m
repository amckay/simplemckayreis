% File: guessHouseholdPolicyRules.m
% Author: Alisdair McKay
% July 19, 2014
% 
% Description: This function creates an initial guess of the household
% policy rules. 
%
% Inputs: none
%
% Ouputs: Vector of parameters for the policy rules.  Each column is a
% discrete type of household and then the parameters for the labor supply
% and savings rules are stacked in the columns.  This matrix is then
% reshaped to a column.
%
%
% Feel free to use, copy or modify this program in any way.



function par0 = guessHouseholdPolicyRules()



global Params ;

    
%initial guess
pars = [0; 0.9*Params.knotXi]; %first element is sup {a :a'(a) = 0}
parn = 0.31*ones(Params.nn,1);   


par0 = [parn; pars]; 
par0 = repmat(par0,1,Params.npp);
            
par0 = par2long(par0);

[ z, M, wage, ii, r, KL, tau] = SteadyStatePrices( );
T = 20;
par0 = egm_stst(par0,1+ii,wage,tau,T);

par0 =par2long(par0);

end

