function  [xaggstst, par, D, Env, X] = compute_stst()
%Finds the steady state of the model


global Params;




[ z, M, wage, ii, r, KL, tau] = SteadyStatePrices(  );



% GIVEN K, SOLVE FOR THE CONSUMPTION FUNCTION BY COLLOCATION:
% high precision in solution of savings function_ is required to achieve
%   converge in outer loop!
[par,check] = broydn(@eulerres_stst,Params.parstart,[1e-11,0,1],1+ii,wage,tau);
if(check~=0)
    %save broyError par;
    warning('compute_stst:broyerror','broydn not converged');
end


Params.parstart = par;
par = par2wide(par);


Pi = forwardmat(1,par);
D = invdistr(Pi);
% D = invd2(Pi);
clear Pi;





xaggstst = agg_stst(D,par);



if nargout > 3
    Env.Dstst = D;
    Env.R = 1+r;
    Env.wage = wage;
    Env.parstst = par;
    Env.aggstst = xaggstst;
end

if nargout > 4
    par = par2long(par);
    X = [zeros(Params.nStatesDistr-1,1);xaggstst;par];
end



end

