% File: SteadyStatePrices.m
% Author: Alisdair McKay
% July 19, 2014
% 
% Description: Returns the prices in steady state
%
%
% Feel free to use, copy or modify this program in any way.
function [ z, M, wage, ii, r, KL, tau] = SteadyStatePrices( )


global Params;


z = 1;
M = 1/Params.mu;
r = 1/Params.betae - 1;  
KL = ((r + Params.delta)/(M*Params.alpha*z))^(1/(Params.alpha-1));
wage =  M*(1-Params.alpha)*z* KL.^Params.alpha;
ii = 1/Params.betae - 1;
tau  = Params.tauBar;

end

