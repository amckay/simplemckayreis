% File: equ.m
% Author: Alisdair McKay
% July 21, 2014
% 
% Description: This file assembles the (many) equations that the model
% solution must satisfy.  These equations are non-linear in this file and
% will be linearized by the program.
%
% Inputs: X is a vector with four components stacked: [current values of
% variables; lagged values of variables; expectational errors; exogenous
% shocks];
%
% Ouputs: resid is residual in model equations.  I.e. the model dictates
% resid = 0.
%
% Input/output: Env is a structure that contains information describing the model
%
%
% Feel free to use, copy or modify this program in any way.
function [resid,Env] = equ(X,Env)

global Params;

ne = NaN;  %suppress a warning.

%unpack
xcurr = X(Env.varix.x);
xlag = X(Env.varix.xlag);
eta = X(Env.varix.eta);
eps = X(Env.varix.eps);

[D,par,dz,xagg,Env] = x2par(xcurr,Env); %#ok
[Dlag,parlag,dzlag,xagglag] = x2par(xlag,Env); %#ok


% unpack stst, xagg and xagglag into local namespace
for nm_i = 1:length(Params.aggnames)
    eval([Params.aggnames{nm_i} '_stst = Env.aggstst(nm_i);'])
    eval([Params.aggnames{nm_i} ' = xagg(nm_i);'])
    eval([Params.aggnames{nm_i} '_L = xagglag(nm_i);'])
end


nc = Params.nc*Params.npp;


%unpack expectational errors
eta_c_hhld = eta(1:nc);
eta_agg = eta(nc+1:nc+Params.nAggEta);




%calculate wages and interest rates
wage =  M*(1-Params.alpha)*z* KL.^Params.alpha;
wage_L =  M_L*(1-Params.alpha)*z_L* KL_L.^Params.alpha;
r =  -Params.delta + M*Params.alpha*z* KL.^(Params.alpha-1);





%rescale household savings by inflation
Dlagscaled = scaleassets(Dlag,(1/ppi));




%household consumption
ch  = expect_C(Dlagscaled,par,1+ii_L,wage,tau);


% ---- aggregate equations (order them with backward, then static then forward) ---

resAgg = initsize(zeros(Params.nagg,1),X);


resAgg(1) = log(z)-Params.rhoz*log(z_L)-eps(1);  %AR(1)
resAgg(2) = -mpshock + Params.rhomp * mpshock_L + eps(2);
resAgg(3) = -markupshock + Params.rhomarkup * markupshock_L + eps(3);
resAgg(4) = be + assetsh - Params.B; % asset market clearing
resAgg(5) = -Params.B -tau*wage*N + Params.B*(1 + ii_L)/ppi; %gov budg



resAgg(6) = KL - K_L/N;  % def of KL
resAgg(7) = Y*S - z * K_L.^Params.alpha * N.^(1-Params.alpha);
resAgg(8) = assetsh - expect_k(D)*Params.nu;  % def of assetsh
resAgg(9) = nh - expect_L(Dlagscaled,par,true)*Params.nu; % def of nh
resAgg(10) = N - nh - ne*Params.skille;  %def of N


resAgg(11) = Params.psi1*ne.^(Params.psi2) - ce.^(-Params.sigma).*Params.skille*wage*(1-tau); %patient household labor supply
resAgg(12) = ce + ch*Params.nu - C;  % def'n of agg C
resAgg(13) = Klag - K_L; %auxilliary 
resAgg(14) = (K + C) - (Y + (1-Params.delta)*K_L - Params.fixedCost -  Params.Psi/2 * ((K-K_L)/K_L).^2 * K_L);  %agg res con
resAgg(15) = K - Inv - (1-Params.delta) * K_L; % capital dynamics
resAgg(16) = -ii + ii_stst + Params.phiP*(ppi-1) +  mpshock; %TAYLOR RULE
resAgg(17) = -ppi + ((1-Params.theta)/(1-Params.theta*pstar.^(1/(1-Params.mu)))).^(1-Params.mu);  % pi
resAgg(18) = -pstar + pbarA/pbarB; %pstar
resAgg(19) = -S + (1-Params.theta)*S_L*ppi.^(-Params.mu/(1-Params.mu))...
    +Params.theta*pstar.^(Params.mu/(1-Params.mu));  % price dispersion
resAgg(20) = iilag - ii_L;%auxilliary 


SDF = Params.betae * ce.^(-Params.sigma) / ce_L.^(-Params.sigma); %patient household stoch discount factor
netI = Inv-Params.delta*K_L;  %def'n of net investment
netI_L = Inv_L - Params.delta*Klag_L;

resAgg(21) =  -SDF*(1 + ii_L)/ppi + 1 + eta_agg(1);  %Euler C
resAgg(22) = -SDF*(1 + r  - (Params.Psi/2)*(netI/K_L).^2 + Params.Psi*(netI/K_L)*(K/K_L))...
    + (1+Params.Psi*(netI_L/Klag_L)) + eta_agg(2); %euler K  
resAgg(23) = -pbarB_L + Y_L + SDF*(1-Params.theta)*ppi.^(-Params.mu/(1-Params.mu)-1)*pbarB +eta_agg(3); % pbarB
resAgg(24) = -pbarA_L + M_L*Params.mu*Y_L ...
    +SDF*(1-Params.theta)*ppi.^(-Params.mu/(1-Params.mu))*pbarA...
    +markupshock_L + eta_agg(4); % pbarA



%dynamics of distribution of wealth
Pi = forwardmat(1,par);
D2 = forward(Dlagscaled,Pi);
resD = distr2par(D2,Env) - distr2par(D,Env);

% household policy rules
resNC = eulerres(parlag,par,1+iilag_L,1+ii_L,wage_L,wage,ppi,tau_L,tau);



% add expectational errors
nemp = Params.npp*Params.nn;

resNC = resNC + [zeros(nemp,1); eta_c_hhld];



resid = [resD;
    resAgg;
    resNC];


