% File: scaleassets.m
% Author: Alisdair McKay
% July 21, 2014
% 
% Description: Takes asset distribution D over Params.knotDistrK and rescales asset
% holdings by scale.
%
% Inputs: D -- distribution of wealth
%   scale -- amount to rescale by
%
% Ouputs: Dscaled -- distribution rescaled.
%
%
% Feel free to use, copy or modify this program in any way.

function Dscaled = scaleassets( D, scale )


global Params;





  nd = Params.ndstst;
  IR = []; IC = []; VV = [];
  for(jp=1:Params.npp)  % next period's state
      
    
    Kend = Params.knotDistrK*scale;
        
   
    
    Ti = lineartrans(Params.knotDistrK,Kend);
    ir{jp} = Ti.iTo;  % row indicates to which position we go
    ic{jp} = Ti.iFr;  % column indicates which position we come from
    vv{jp} = Ti.Val;
    
    
    Pij{jp} = sparse(ir{jp},ic{jp},vv{jp},nd,nd);
    
  end
  

Pi = blockdiag(Pij);

Dscaled = Pi * D;
  

end