
% Computes the effective labor supply,
% Inputs:
%   pvec:  vector of probabilities (histogram weights)
%   par: policy rule parameters
%   prodWeight: if true (default), weight hours worked by productivity.

function EL = expect_L(pvec,par,prodWeight)
  global Params;
  
  ndk = Params.ndstst;
  
  EL = 0;
  
  for ip=1:Params.npp
      N = nspline(par(Params.par_nind,ip));
      if nargin < 3 || prodWeight
          prod_scale = Params.skill(ip);
      else
          prod_scale = Params.skill(ip)>1e-6;
      end
     
      nsup = interp_nspline(N,Params.knotDistrK,true) * prod_scale;
      offs = (ip-1)*ndk;
      EL = EL + sum(pvec(offs+1:offs+ndk) .* nsup);
  end
  
  assert(ip*ndk == length(pvec));
      
  
