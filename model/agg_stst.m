% File: agg_stst.m
% Author: Alisdair McKay
% July 21, 2014
% 
% Description: Solves for the steady state given household policy rules (par) and
% diststibution of wealth (D)
%
% Ouputs: vector of variable values at steady state (xaggstst) the order of
% variables is contained in Params.aggnames.  I.e. xaggstst(1) corresponds
% to the variable whose name appears in Params.aggnames{1}
%
%
% Feel free to use, copy or modify this program in any way.


function xaggstst= agg_stst(D,par)


ne = fzero(@check_stst,0.3,[],D,par);
[~, xaggstst]=check_stst(ne,D,par);


end




function [laborResid, xaggstst]=check_stst(ne,D,par)

global Params;


%solve for steady state

mpshock = 0; %#ok
markupshock = 0; %#ok
S = 1; %#ok
ppi = 1; %#ok
pstar = 1;%#ok


[ z, M, wage, ii, r, KL, tau] = SteadyStatePrices( );
YonN = KL^Params.alpha;



%hhld variables:

%total income of households:
bh = expect_k(D) * Params.nu;  %this is end of period assets
assetsh = bh;
nh = expect_L(D,par) * Params.nu;
ch = expect_C(D,par,1+ii,wage,tau);



N = nh + Params.skille*ne;
Y = YonN * N;
K = KL * N;
Inv = Params.delta * K;
C = Y - Params.delta*K - Params.fixedCost;
ce = C- ch * Params.nu;
dividend = (1-M)*Y - Params.fixedCost;


%other


laborResid =  Params.psi1 .* ne.^Params.psi2  ./(ce.^(-Params.sigma)...
    .*Params.skille*wage.*(1-tau))-1;

if nargout < 2
    return
end


xaggstst = zeros(Params.nagg,1);

Params.B = (tau * N * wage)/ii;
be = Params.B - bh;


pbarA = Y/(1-Params.betae*(1-Params.theta));
pbarB = pbarA; %#ok


Klag = K; %#ok
iilag = ii;



for i = 1:length(Params.aggnames)
    eval(['xaggstst(' num2str(i) ') = ' Params.aggnames{i} ';']);
end



end

