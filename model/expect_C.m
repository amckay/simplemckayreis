% [EC Emargtax Etaxpaid] = expect_C(pvec,par,R,wage)
% Computes the aggregate consumption and taxes of households
% Inputs:
%   pvec:  vector of probabilities (histogram weights)
%   par: policy rule parameters
%   R: interest rate
%   wage
%   disc: vector of discrete states levels over which to integrate
%
% Alisdair McKay 2/24/12

function [EC, varlogc] = expect_C(pvec,par,R,wage,tau,disc)

  global Params;

  
 
  if ~exist('disc','var')
      disc = [];
  end
  
  npp = Params.npp;
  par = par2wide(par);
  
  ndk = Params.ndstst;
  
  EC = 0;
  varlogc = 0;
  Elogc = 0;

  
  if isempty(disc)  % use all skill levels
      dgrid = 1:npp;
  else
      dgrid = disc;
  end
  
    
  %loop over discrete grid
  for ip=dgrid
      S = savingspline(par(Params.par_sind,ip));
      N = nspline(par(Params.par_nind,ip));
    
      %we have end of period assets in last period and this period,
      %and we have to figure out this period's consumption:
      bthis = Params.knotDistrK;
      xthis = interp_savspline(S,bthis);
      c = get_cnt(bthis,xthis,N,R,wage,tau,ip);
      
      
      
      %build up expectation
      offs = (ip-1)*ndk;
           
      EC = EC + sum(pvec(offs+1:offs+ndk) .* c);
      
      
      if nargout >= 4
          Elogc = Elogc + sum(pvec(offs+1:offs+ndk) .* log(c));
          varlogc = varlogc + sum(pvec(offs+1:offs+ndk) .* log(c).^2);
      end
  end
  
  varlogc = varlogc - Elogc^2;

      
  
end
