% File: initparams.m
% Author: Alisdair McKay
% July 19, 2014
% 
% Description: This file sets the model parameters in a global variable
% called Params.


global Params;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Scalars
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%preferences
Params.betah = 0.978961862738;  %impatient household discount factor
Params.sigma = 1;     %risk aversion
Params.psi1 = 19.6855103766; % labor supply parameter
Params.psi2 = 2; % labor supply parameter
Params.betae = 0.988830128237; %patient discount factor



%markets
Params.mu = 1.2;
Params.theta = 1/3.5;

%production and technology
Params.fixedCost = 0.57528560756;
Params.alpha = 1 - (1-0.36) * Params.mu;  % capital share
Params.delta = 0.0456/4; % depreciation

Params.Psi =  6;   %capital adj cost




%government
Params.tauBar = 0.1;
Params.phiP = 1.55;


%patient/impatient distribution
Params.nu = 4;
Params.skille = 3.71838356684; %see calibrate.m

% aggregate shocks
Params.SigmaEpsz = 0.002936169743355;  % st dev of z shock
Params.rhoz = 0.75; % persistence of z shock


Params.SigmaEpsmp = 0.003533607840688; % st dev of MP shock
Params.rhomp = 0.62; % persistence


Params.Sigmamarkup =  0.1;
Params.rhomarkup = 0.85;

Params.Sigma = diag([Params.SigmaEpsz; Params.SigmaEpsmp; Params.Sigmamarkup]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Household heterogeneity and income process
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Params.npp = 3; %number of discrete household types.
                %you can use this to control "big" model with npp = 9
                %or small model (no skill differences) with npp = 3
                

Params.skill = [0.2 1 5];
Params.skillTrans = [0.97 0.03 0; 0.03 0.94 0.03; 0 0.03 0.97];  %(i,j) element is prob from i to j 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Approximation of policy rules and asset positions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Params.nc = 100; %number of points in approximation of individual cons func
Params.nn = 100; %number of points in approximation of individual labor supply func
Params.ncnv = Params.nc + Params.nn;

Params.ndstst = 250; %number of points in wealth histogram

Params.bhmin = 0;  %min bond position for household
Params.bhmax = 200;  %max bond position for household





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% quadrature grid for_ wealth distribution:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Params.nStatesDistr = Params.ndstst*Params.npp;  %makes only sense for npp==1; otherwise_:DSF!
% number knot points:
ndk = Params.ndstst ;
[Params.knotDistrK,Params.logshift] = makeknotd(Params.bhmin,Params.bhmax,ndk);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% KNOT POINTS FOR_ SAVINGS POLYNOMIAL:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Params.xmin = 0.001;
Params.xmax = 0.5*Params.bhmax; % beyond that point, linear extrapolation is fine!

 n1 = ceil(Params.nc/3);
 x1 = linspace(Params.xmin,0.2,n1+1)';
 x2 = logspaceshift(0.2,Params.xmax,Params.nc-n1,Params.logshift)';
 Params.knotXi = [x1(1:end-1);x2];

%  Params.knotXi = logspaceshift(Params.xmin,Params.xmax,Params.nc,Params.logshift)';


Params.knotXi = Params.knotXi(2:end);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% KNOT POINTS FOR_ LABOR SUPPLY POLYNOMIAL:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Params.nxmin = 0.001;
Params.nxmax = Params.bhmax;
n1 = ceil(Params.nn/3);
x1 = linspace(Params.nxmin,0.2,n1+1)';
x2 = logspaceshift(0.2,Params.nxmax,Params.nn-n1,Params.logshift)';
Params.ngrid = [x1(1:end-1);x2];




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Aggregate variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Params.nz = 3; % number of exogenous var

Params.nAggEta = 4;  


  

%Names of aggregate variables (classification is not particularly important)
exog = {'z', 'mpshock', 'markupshock'};
other_states = {'be', 'K'};
static = {'KL', 'Y', 'N', 'assetsh',  'nh', 'ne',...
     'C', 'pstar', 'ppi', 'Klag', 'S',  'M', 'tau', 'ii','iilag'};
dec = { 'ce', 'Inv', 'pbarA', 'pbarB'};

Params.aggnames = {exog{:}, other_states{:}, static{:}, dec{:}};


Params.nagg = length(Params.aggnames);


% create indices
n = length(exog)+length(other_states);
Params.aggInd.state = logical([ones(1,n) zeros(1,Params.nagg-n)]);

n = length(dec);
Params.aggInd.dec = logical([zeros(1,Params.nagg-n) ones(1,n)]);

n = length(exog);
Params.aggInd.exog = logical([ones(1,n) zeros(1,Params.nagg-n)]);

Params.aggInd.static = ~logical(Params.aggInd.state + Params.aggInd.dec);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% indexing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Params.par_sind = Params.nn+1:+Params.nn+Params.nc;
Params.par_nind = 1:Params.nn;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Options for the solution algorthim
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Params.IRF_Length = 40;
Params.sim_T = 10000;
Params.seed = 4239074;