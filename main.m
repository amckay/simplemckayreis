% File: main.m
% Author: Alisdair McKay
% July 19, 2014
% 
% Description: This program is the main script.  It solves the model,
% calculates moments and computes impulse response functions.
%
% Feel free to use, copy or modify this program in any way.

clear all; close all; clc;

% set the Matlab path
setpath;

  
%set the baseline parameters
initparams;
 

%initial guess of policy rules
Params.parstart = guessHouseholdPolicyRules();


%% COMPUTE STEADY STATE:

[xaggstst, par, D, Env, X] = compute_stst();

%% CHECK EULER RESIDUALS IN STEADY STATE:

if true
    nxtest = 1000;
    xx2 = linspace(Params.xmin,Params.xmax,nxtest)';

    [ z, M, wage, ii, r, KL, tau] = SteadyStatePrices(  );

    
    ppinext = 1;
    
    
    resC = eulerres(par,par,1+ii,1+ii,wage,wage,ppinext,tau,tau,xx2);
    ndec = 2;
    
    
    resC = reshape(resC,ndec*nxtest,Params.npp);

    nresplot = ndec*Params.npp;
    for ip=1:nresplot
        subplot(Params.npp,ndec,ip);
        plot(xx2,resC(1+nxtest*(ip-1):nxtest*ip));
        %ylim([-5e-4 5e-4]);

    end

end

%% Analyze the steady state



% ***compute statistics of wealth distribution***

%household wealth is just bond wealth
b = Params.knotDistrK;
b = repmat(b,Params.npp,1);

pop = Params.nu * D;  % this is the mass of households in each wealth cell

%patient household wealth is 
wealthCO = xaggstst(strcmp(Params.aggnames,'be')) + xaggstst(strcmp(Params.aggnames,'K'));


[giniIndex, Lorenz] = gini([pop; 1], [b; wealthCO], false);



disp(['Gini coefficient is ' num2str(giniIndex)]);
disp('Wealth quintiles:')
disp('quintile    share of wealth')
disp(['1           ' num2str(...
    Lorenz(find(Lorenz(:,1) <= 0.2,1,'last'),2) - 0 ...
    )])
disp(['2           ' num2str(...
    Lorenz((find(Lorenz(:,1) <= 0.4,1,'last')),2) - Lorenz(find(Lorenz(:,1) <= 0.2,1,'last'),2)...
    )])
disp(['3           ' num2str(...
    Lorenz(find(Lorenz(:,1) <= 0.6,1,'last'),2) - Lorenz(find(Lorenz(:,1) <= 0.4,1,'last'),2)...
    )])
disp(['4           ' num2str(...
    Lorenz(find(Lorenz(:,1) <= 0.8,1,'last'),2) - Lorenz(find(Lorenz(:,1) <= 0.6,1,'last'),2)...
    )])
disp(['5           ' num2str(...
    1 - Lorenz(find(Lorenz(:,1) <= 0.8,1,'last'),2)...
    )])


clear Lorenz giniIndex pop b wealthCO;




%% Linearization


Params.ntotal = length(X);

% the number of expectational errors
neta = Params.nc*Params.npp + Params.nAggEta;


[X2,varindx] = setix(X,neta,Params.nz); 

disp('Linearizing model equations')
Env.varix = varindx;
[residStst,Env] = equ(X2,Env);
if any(abs(residStst) >1e-6)
    error('wrong stst');
end
  

njac = 20*ones(size(Env.varix.x));
njac(Env.iVarStatic)=500;
Env = dojacob(@equ,X2,Env,njac,Env);


%% solve the model

disp('Solving the model')
[G1,impact,eu,abseig] = solveexact(Env);
if(~all(eu==1))  %signal failure:
    G1 = [];
    warning('sims:fails','sims failed');
end
    

%% Create observation matrix for all aggregate variables 

iA = Env.iVarAggr;  %might need to switch to iVarAggrBWS when we do do_mpa
niA = length(iA);

Hagg = sparse(1:niA,iA,ones(1,niA),niA,Params.ntotal)';
   


%% Compute IRFs

irf_agg = ir_sims(G1,impact,Params.IRF_Length-1,Hagg',Params.Sigma); 


%% compute variances through simulation
disp('Computing variances through simulation')


[ser, shocks] = simulateSystem(G1,impact,Hagg',Params.sim_T,Params.Sigma,Params.seed);
ser = ser +  Env.aggstst * ones(1,Params.sim_T);
V0 = cov(log(ser'));
simul_avg = mean(ser,2);


%% plot and display some of the results

iY = find(strcmp(Params.aggnames,'Y'));
iC = find(strcmp(Params.aggnames,'C'));
ippi = find(strcmp(Params.aggnames,'ppi'));
iN =  find(strcmp(Params.aggnames,'N'));

disp('-----------------RESULTS------------------')
disp(['variance of log output ' num2str(V0(iY,iY))]);
disp(['variance of log consumption ' num2str(V0(iC,iC))]);
disp(['correlation of log consumption and log output ' num2str(V0(iY,iC)/sqrt(V0(iY,iY)*V0(iC,iC)))]);


figure
subplot(2,2,1)
plot([irf_agg{1}(:,iY) irf_agg{2}(:,iY) irf_agg{3}(:,iY)]./Env.aggstst(iY))
legend('Tech.', 'Mon. Pol.', 'Markup','Location','Best')
title('Output')
subplot(2,2,2)
plot([irf_agg{1}(:,iC) irf_agg{2}(:,iC) irf_agg{3}(:,iC)]./Env.aggstst(iC))
legend('Tech.', 'Mon. Pol.', 'Markup','Location','Best')
title('consumption')
subplot(2,2,3)
plot([irf_agg{1}(:,ippi) irf_agg{2}(:,ippi) irf_agg{3}(:,ippi)])
legend('Tech.', 'Mon. Pol.', 'Markup','Location','Best')
title('inflation')
subplot(2,2,4)
plot([irf_agg{1}(:,iN) irf_agg{2}(:,iN) irf_agg{3}(:,iN)])
legend('Tech.', 'Mon. Pol.', 'Markup','Location','Best')
title('labor input (skill-weighted)')