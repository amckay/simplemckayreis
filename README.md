This repository contains Matlab codes to solve a simplified version of the model in "The role of automatic stabilizers in the US business cycle" by Alisdair McKay and Ricardo Reis.

The simplified model description can be found at:
http://people.bu.edu/amckay/pdfs/MR_Simplified.pdf

To begin:
1) download/clone this repository.  To download, click on the cloud at left.
2) cd into repository directory.
3) run main.m

Acknowledgements: this repository contains codes written by others notably Michael Reiter.  If you make use of these files please give credit where it is due.  

Send questions to: amckay@bu.edu